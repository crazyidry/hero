<?php

require "vendor/autoload.php";

$heroService = new \Hero\Services\HeroService();
$builderService = new \Hero\Builders\HeroBuilder();

$logger = new \Monolog\Logger("display_log");
$logger->setHandlers([new \Monolog\Handler\ErrorLogHandler()]);
$displayService = new \Hero\Services\DisplayService($logger);

$game = new \Hero\HeroGame($heroService, $builderService, $displayService);

$game->gameloop();