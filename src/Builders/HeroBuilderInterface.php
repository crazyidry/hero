<?php


namespace Hero\Builders;


use Hero\Characters\Beast;

interface HeroBuilderInterface
{
    public function build($characterClass, $characterSpec): Beast;

    public function buildRandom($characterClass, $characterSpecLimits): Beast;
}