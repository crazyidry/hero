<?php


namespace Hero\Builders;


use Hero\Characters\Beast;

class HeroBuilder implements HeroBuilderInterface
{
    public function build($characterClass, $characterSpec): Beast
    {
        $character = new $characterClass;
        foreach($characterSpec as $field => $value) {
            $character->{'set' . ucfirst($field)}($value);
        }
        return $character;
    }

    protected function generateSpecs($characterSpecLimits)
    {
        $specs = [];
        foreach($characterSpecLimits as $spec => $limits) {
            if (is_array($limits)) {
                list($min, $max) = $limits;
                $specs[$spec] = $this->generateSpec($min, $max);
            } else {
                $specs[$spec] = $limits;
            }
        }
        return $specs;
    }

    protected function generateSpec($min, $max) {
        return rand($min, $max);
    }


    public function buildRandom($characterClass, $characterSpecLimits): Beast
    {
        return $this->build($characterClass, $this->generateSpecs($characterSpecLimits));
    }
}