<?php

namespace Hero\Services;

use Hero\Characters\Beast;
use Hero\Characters\Hero;
use Monolog\Handler\ErrorLogHandler;
use Monolog\Logger;
use Psr\Log\LoggerInterface;

class HeroService implements HeroServiceInterface
{
    /**
     * @param Hero $hero
     * @param Beast $beast
     * @return Beast[]
     */
    public function calculateFirsStriker(Hero $hero, Beast $beast)
    {
        switch(true) {
            case $hero->getSpeed() > $beast->getSpeed():
            case $hero->getSpeed() == $beast->getSpeed() && $hero->getLuck() > $beast->getLuck():
                return [$hero, $beast];
            default:
                return [$beast, $hero];
        }
    }

    /**
     * @param Beast $striker
     * @param Beast $defender
     * @return Beast[]
     */
    public function changeStriker(Beast $striker, Beast $defender)
    {
        return [$defender, $striker];
    }

    /**
     * @param Beast $striker
     * @param Beast $defender
     * @return Beast[]
     */
    public function strike(Beast $striker, Beast $defender)
    {
        $striker = clone $striker;
        $defender = clone $defender;
        $defender->setHealth($defender->getHealth() + $this->calculateDefence($defender) - $this->calculateMagicShield($defender) * $this->calculateLuck($defender) * $this->calculateAtack($striker));
        return [$striker, $defender];
    }

    /**
     * @param Beast $striker
     * @return int
     */
    public function calculateAtack(Beast $striker)
    {
        $atack = $striker->getStrength();
        return ($striker instanceof Hero && rand(1, 100) <= $striker->getChanceOfDoubleStrike()) ? 2 * $atack : $atack;
    }

    /**
     * @param Beast $striker
     * @return int
     */
    public function calculateDefence(Beast $defender)
    {
        return $defender->getDefence();
    }

    /**
     * @param Beast $defender
     * @return float|int
     */
    public function calculateMagicShield(Beast $defender)
    {
        return ($defender instanceof Hero && rand(1, 100) <= $defender->getChanceOfMagicShield()) ? 0.5: 1;
    }

    /**
     * @param Beast $defender
     * @return int
     */
    public function calculateLuck(Beast $defender)
    {
        return (rand(1, 100) <= $defender->getLuck()) ? 0 : 1;
    }

    /**
     * @param Beast $beast
     * @return bool
     */
    public function canContinueFight(Beast $beast)
    {
        return $beast->getHealth() > 0;
    }
}