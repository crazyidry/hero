<?php


namespace Hero\Services;


use Hero\Characters\Beast;
use Hero\Characters\Hero;

interface DisplayServiceInterface
{
    /**
     * @param Hero $hero
     * @param Beast $beast
     */
    public function displayCalculateFirsStriker(Beast $beast);

    /**
     * @param Beast $striker
     * @param Beast $defender
     */
    public function displayChangeStriker($round, Beast $striker, Beast $defender);

    /**
     * @param Beast $striker
     * @param Beast $defender
     */
    public function displayStrike($round, Beast $striker, Beast $defender);

    /**
     * @param $rounds
     * @param Beast $striker
     * @param Beast $defender
     */
    public function displayEndGame($round, Beast $striker, Beast $defender);

    /**
     * @param Hero $hero
     * @param Beast $beast
     */
    public function displayInitialize(Hero $hero, Beast $beast);
}