<?php


namespace Hero\Services;


use Hero\Characters\Beast;
use Hero\Characters\Hero;
use Psr\Log\LoggerInterface;

class DisplayService implements DisplayServiceInterface
{
    /**
     * @var LoggerInterface
     */
    protected $logService;

    /**
     * DisplayService constructor.
     * @param LoggerInterface $logService
     */
    public function __construct(LoggerInterface $logService)
    {
        $this->logService = $logService;
    }

    public function displayCalculateFirsStriker(Beast $beast)
    {
        $this->logService->info(sprintf("First striker is: %s", $beast));
    }

    public function displayChangeStriker($round, Beast $striker, Beast $defender)
    {
        $this->logService->info(sprintf("New Striker: %s", $striker));
        $this->logService->info(sprintf("New Defender: %s", $defender));
    }

    public function displayStrike($round, Beast $striker, Beast $defender)
    {
        $this->logService->info(sprintf("Round[%s] Strikes: %s", $round, $striker));
        $this->logService->info(sprintf("Round[%s] Defended: %s", $round, $defender));
    }

    public function displayEndGame($round, Beast $striker, Beast $defender)
    {
        $this->logService->info(sprintf("This fight ended in %s rounds.", $round));
    }

    public function displayInitialize(Hero $hero, Beast $beast)
    {
        $this->logService->info(sprintf("Hero generated: %s", $hero));
        $this->logService->info(sprintf("Beast generated: %s", $beast));
    }
}