<?php


namespace Hero\Services;


use Hero\Characters\Beast;
use Hero\Characters\Hero;

interface HeroServiceInterface
{
    /**
     * @param Hero $hero
     * @param Beast $beast
     * @return Beast[]
     */
    public function calculateFirsStriker(Hero $hero, Beast $beast);

    /**
     * @param Beast $striker
     * @param Beast $defender
     * @return Beast[]
     */
    public function changeStriker(Beast $striker, Beast $defender);

    /**
     * @param Beast $striker
     * @param Beast $defender
     * @return Beast[]
     */
    public function strike(Beast $striker, Beast $defender);

    /**
     * @param Beast $beast
     * @return bool
     */
    public function canContinueFight(Beast $beast);
}