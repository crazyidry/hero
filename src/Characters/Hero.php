<?php


namespace Hero\Characters;


class Hero extends Beast
{
    /**
     * @var int
     */
    protected $chanceOfDoubleStrike;

    /**
     * @var int
     */
    protected $chanceOfMagicShield;

    /**
     * @return int
     */
    public function getChanceOfDoubleStrike()
    {
        return $this->chanceOfDoubleStrike;
    }

    /**
     * @param int $chanceOfDoubleStrike
     */
    public function setChanceOfDoubleStrike($chanceOfDoubleStrike)
    {
        $this->chanceOfDoubleStrike = $chanceOfDoubleStrike;
    }

    /**
     * @return int
     */
    public function getChanceOfMagicShield()
    {
        return $this->chanceOfMagicShield;
    }

    /**
     * @param int $chanceOfMagicShield
     */
    public function setChanceOfMagicShield($chanceOfMagicShield)
    {
        $this->chanceOfMagicShield = $chanceOfMagicShield;
    }

    public function __toString()
    {
        return json_encode([
            "class" => self::class,
            "health" => $this->health,
            "strength" => $this->strength,
            "speed" => $this->speed,
            "defence" => $this->defence,
            "luck" => $this->luck,
            "chanceOfMagicShield" => $this->chanceOfMagicShield,
            "chanceOfDoubleStrike" => $this->chanceOfDoubleStrike
        ]);
    }
}