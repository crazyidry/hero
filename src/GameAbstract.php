<?php

namespace Hero;

abstract class GameAbstract
{
    public function gameloop()
    {
        $this->initialize();
        while(!$this->endGame()) {
            $this->nextMove();
            $this->changeTurn();
        }
    }

    public abstract function initialize();

    public abstract function endGame();

    public abstract function nextMove();

    public abstract function changeTurn();
}