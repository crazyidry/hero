<?php

namespace Hero;

use Hero\Builders\HeroBuilder;
use Hero\Builders\HeroBuilderInterface;
use Hero\Characters\Beast;
use Hero\Characters\Hero;
use Hero\Services\DisplayService;
use Hero\Services\DisplayServiceInterface;
use Hero\Services\HeroService;
use Hero\Services\HeroServiceInterface;

class HeroGame extends GameAbstract
{
    /**
     * @var HeroServiceInterface
     */
    protected $heroService;

    /**
     * @var HeroBuilderInterface
     */
    protected $heroBuilder;

    /**
     * @var DisplayService
     */
    protected $displayService;

    /**
     * @var array
     */
    protected $heroSpecs;

    /**
     * @var array
     */
    protected $beastSpecs;

    /**
     * @var Beast|null
     */
    protected $striker = null;

    /**
     * @var Beast|null
     */
    protected $defender = null;

    /**
     * @var Hero
     */
    protected $hero;

    /**
     * @var Beast
     */
    protected $beast;

    /**
     * @var int
     */
    protected $rounds;

    /**
     * @var int
     */
    protected $totalRounds;

    /**
     * HeroGame constructor.
     * @param HeroService $heroService
     */
    public function __construct(HeroServiceInterface $heroService, HeroBuilderInterface $heroBuilder, DisplayServiceInterface $displayService, $rounds = 20, array $heroSpecs = [], array $beastSpecs = [])
    {
        $this->heroService = $heroService;
        $this->heroBuilder = $heroBuilder;
        $this->displayService = $displayService;
        $this->heroSpecs = (!empty($heroSpecs)) ? $heroSpecs : [
            "health" => 100,
            "strength" => 70,
            "speed" => 45,
            "defence" => 55,
            "luck" => 10,
            "chanceOfMagicShield" => 10,
            "chanceOfDoubleStrike" => 20
        ];
        $this->beastSpecs = (!empty($beastSpecs)) ? $beastSpecs : [
            "health" => 90,
            "strength" => 70,
            "speed" => 60,
            "defence" => 50,
            "luck" => 25
        ];
        $this->rounds = $rounds;
        $this->totalRounds = $rounds;
    }

    public function initialize()
    {
        $this->hero = $this->heroBuilder->buildRandom(Hero::class, $this->heroSpecs);
        $this->beast = $this->heroBuilder->buildRandom(Beast::class, $this->beastSpecs);
        $this->displayService->displayInitialize($this->hero, $this->beast);
    }

    public function endGame()
    {
        $gameEnded = $this->rounds <= 0 ||
            !(is_null($this->defender) || $this->heroService->canContinueFight($this->defender)) ||
            !(is_null($this->striker) || $this->heroService->canContinueFight($this->striker));
        if ($gameEnded) {
            $this->displayService->displayEndGame($this->totalRounds - $this->rounds, $this->striker, $this->defender);
        }
        return $gameEnded;
    }

    public function nextMove()
    {
        if (is_null($this->striker)) {
            list($this->striker, $this->defender ) = $this->heroService->calculateFirsStriker($this->hero, $this->beast);
            $this->displayService->displayCalculateFirsStriker($this->striker);
        }

        list($this->striker, $this->defender ) = $this->heroService->strike($this->striker, $this->defender);
        $this->displayService->displayStrike($this->totalRounds - $this->rounds, $this->striker, $this->defender);
        $this->rounds--;
    }

    public function changeTurn()
    {
       list($this->striker, $this->defender) = $this->heroService->changeStriker($this->striker, $this->defender);
       $this->displayService->displayChangeStriker($this->totalRounds - $this->rounds, $this->striker, $this->defender);
    }

    /**
     * @return Beast|null
     */
    public function getStriker()
    {
        return $this->striker;
    }

    /**
     * @param Beast|null $striker
     */
    public function setStriker($striker)
    {
        $this->striker = $striker;
    }

    /**
     * @return Hero
     */
    public function getHero()
    {
        return $this->hero;
    }

    /**
     * @param Hero $hero
     */
    public function setHero($hero)
    {
        $this->hero = $hero;
    }

    /**
     * @return Beast
     */
    public function getBeast()
    {
        return $this->beast;
    }

    /**
     * @param Beast $beast
     */
    public function setBeast($beast)
    {
        $this->beast = $beast;
    }

    /**
     * @return int
     */
    public function getRounds()
    {
        return $this->rounds;
    }

    /**
     * @param int $rounds
     */
    public function setRounds($rounds)
    {
        $this->rounds = $rounds;
    }

    /**
     * @return HeroService
     */
    public function getHeroService()
    {
        return $this->heroService;
    }

    /**
     * @param HeroService $heroService
     */
    public function setHeroService($heroService)
    {
        $this->heroService = $heroService;
    }

    /**
     * @return Beast|null
     */
    public function getDefender()
    {
        return $this->defender;
    }

    /**
     * @param Beast|null $defender
     */
    public function setDefender($defender)
    {
        $this->defender = $defender;
    }

}