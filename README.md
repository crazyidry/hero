# Hero Game - eMAG 

Here is a small How To for the game.

## Installation

To install run the following command

```bash
$ composer install
```

## Basic Usage

```bash
$ php run.php
```

## The tests

```bash
$ ./vendor/bin/phpunit
```