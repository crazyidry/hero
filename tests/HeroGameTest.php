<?php

namespace HeroTest;


use Hero\Builders\HeroBuilder;
use Hero\Characters\Beast;
use Hero\Characters\Hero;
use Hero\HeroGame;
use Hero\Services\DisplayService;
use Hero\Services\HeroService;
use Monolog\Handler\ErrorLogHandler;
use Monolog\Logger;
use PHPUnit\Framework\TestCase;

class HeroGameTest extends TestCase
{
    public function testTheGameloop ()
    {
        $game = new HeroGame(new HeroService(), new HeroBuilder(), $this->getMockDisplayService(), 10, [
            "health" => 100,
            "strength" => 70,
            "speed" => 45,
            "defence" => 55,
            "luck" => 10
        ], [
            "health" => 90,
            "strength" => 70,
            "speed" => 60,
            "defence" => 50,
            "luck" => 25
        ]);

        $game->gameloop();

        $this->assertTrue($game->getHero()->getHealth() == 100);
        $this->assertTrue($game->getBeast()->getHealth() == 90);
        $this->assertTrue($game->getRounds() < 10);
    }

    public function testInitialization()
    {
        $heroService = $this->getMockBuilder(HeroService::class)->getMock();
        $game = new HeroGame($heroService, new HeroBuilder(), $this->getMockDisplayService());
        $game->initialize();

        $this->assertTrue($game->getHero()->getHealth() == 100);
        $this->assertTrue($game->getBeast()->getHealth() == 90);
        $this->assertTrue($game->getHero()->getStrength() == 70);
        $this->assertTrue($game->getBeast()->getStrength() == 70);
        $this->assertTrue($game->getRounds() == 20);
    }

    protected function getMockDisplayService()
    {
        $logger = new Logger("basic_logger");
        $logger->setHandlers([new ErrorLogHandler()]);
        $displayService = $this->getMockBuilder(DisplayService::class)
            ->setConstructorArgs([$logger])
            ->setMethods(["displayCalculateFirsStriker", "displayChangeStriker", "displayStrike", "displayEndGame", "displayInitialize"])
            ->getMock();
        return $displayService;
    }

    public function testEndGame()
    {
        $heroService = new HeroService();
        $game = new HeroGame($heroService, new HeroBuilder(), $this->getMockDisplayService());

        $this->assertFalse($game->endGame());

        $game->initialize();
        $game->setStriker($game->getBeast());
        $game->setDefender($game->getHero());

        $this->assertFalse($game->endGame());

        $game->getDefender()->setHealth(0);
        $this->assertTrue($game->endGame());

        $game->getDefender()->setHealth(10);
        $game->getStriker()->setHealth(0);
        $this->assertTrue($game->endGame());

        $game->getDefender()->setHealth(10);
        $game->getStriker()->setHealth(-10);
        $this->assertTrue($game->endGame());

        $game->getDefender()->setHealth(-10);
        $game->getStriker()->setHealth(10);
        $this->assertTrue($game->endGame());

        $game->getDefender()->setHealth(10);
        $game->getStriker()->setHealth(10);
        $game->setRounds(0);
        $this->assertTrue($game->endGame());

        $game->setRounds(-1);
        $this->assertTrue($game->endGame());
    }

    public function testNextMove()
    {
        $heroService = new HeroService();
        $game = new HeroGame($heroService, new HeroBuilder(), $this->getMockDisplayService());

        $hero = new Hero();
        $hero->setHealth(100);
        $hero->setStrength(70);
        $hero->setSpeed(50);

        $beast = new Beast();
        $beast->setHealth(90);
        $beast->setStrength(70);
        $beast->setSpeed(60);

        $game->setHero($hero);
        $game->setBeast($beast);

        $game->nextMove();

        $this->assertTrue($game->getDefender()->getHealth() == 30);
    }

    public function testChangeTurn()
    {
        $heroService = new HeroService();
        $game = new HeroGame($heroService, new HeroBuilder(), $this->getMockDisplayService());

        $game->initialize();
        $game->setStriker($game->getHero());
        $game->setDefender($game->getBeast());

        $game->changeTurn();
        $this->assertTrue($game->getStriker() . "" == $game->getBeast() . "");
        $this->assertTrue($game->getDefender() . "" == $game->getHero() . "");
    }
}