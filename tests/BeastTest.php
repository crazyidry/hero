<?php


namespace HeroTest;

use Hero\Characters\Beast;
use PHPUnit\Framework\TestCase;

class BeastTest extends TestCase
{
    public function testClassExists()
    {
        $this->assertTrue(class_exists(Beast::class));
    }
}