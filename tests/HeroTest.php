<?php


namespace HeroTest;


use Hero\Characters\Beast;
use Hero\Characters\Hero;
use PHPUnit\Framework\TestCase;

class HeroTest extends TestCase
{
    public function testClassExists()
    {
        $this->assertTrue(class_exists(Hero::class));
    }

    public function testHeroExtendsBeast()
    {
        $hero = new Hero();
        $this->assertTrue($hero instanceof Beast);
    }
}