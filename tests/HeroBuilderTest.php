<?php


namespace HeroTest;


use Hero\Builders\HeroBuilder;
use Hero\Characters\Beast;
use Hero\Characters\Hero;
use PHPUnit\Framework\TestCase;

class HeroBuilderTest extends TestCase
{
    public function testBuild()
    {
        $builder = new HeroBuilder();

        $hero = $builder->build(Hero::class, [
            "health" => 101,
            "strength" => 102,
            "speed" => 103,
            "luck" => 104,
            "defence" => 105
        ]);

        $this->assertTrue($hero->getHealth() == 101);
        $this->assertTrue($hero->getStrength() == 102);
        $this->assertTrue($hero->getSpeed() == 103);
        $this->assertTrue($hero->getLuck() == 104);
        $this->assertTrue($hero->getDefence() == 105);

        $beast = $builder->build(Beast::class, [
            "health" => 101,
            "strength" => 102,
            "speed" => 103,
            "luck" => 104,
            "defence" => 105
        ]);

        $this->assertTrue($beast->getHealth() == 101);
        $this->assertTrue($beast->getStrength() == 102);
        $this->assertTrue($beast->getSpeed() == 103);
        $this->assertTrue($beast->getLuck() == 104);
        $this->assertTrue($beast->getDefence() == 105);
    }

    public function testBuildRandom()
    {
        $builder = new HeroBuilder();

        $min = 10;
        $max = 20;

        $hero = $builder->buildRandom(Hero::class, [
            "health" => [$min, $max],
            "strength" => [$min, $max],
            "speed" => [$min, $max],
            "luck" => [$min, $max],
            "defence" => [$min, $max]
        ]);

        $this->assertTrue($hero->getHealth() >= $min && $hero->getHealth() <= $max);
        $this->assertTrue($hero->getStrength() >= $min && $hero->getStrength() <= $max);
        $this->assertTrue($hero->getSpeed() >= $min && $hero->getSpeed() <= $max);
        $this->assertTrue($hero->getLuck() >= $min && $hero->getLuck() <= $max);
        $this->assertTrue($hero->getDefence() >= $min && $hero->getDefence() <= $max);

        $beast = $builder->buildRandom(Beast::class, [
            "health" => [$min, $max],
            "strength" => [$min, $max],
            "speed" => [$min, $max],
            "luck" => [$min, $max],
            "defence" => [$min, $max]
        ]);

        $this->assertTrue($beast->getHealth() >= $min && $beast->getHealth() <= $max);
        $this->assertTrue($beast->getStrength() >= $min && $beast->getStrength() <= $max);
        $this->assertTrue($beast->getSpeed() >= $min && $beast->getSpeed() <= $max);
        $this->assertTrue($beast->getLuck() >= $min && $beast->getLuck() <= $max);
        $this->assertTrue($beast->getDefence() >= $min && $beast->getDefence() <= $max);
    }
}