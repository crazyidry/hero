<?php


namespace HeroTest;


use Hero\Characters\Beast;
use Hero\Characters\Hero;
use Hero\HeroGame;
use Hero\Services\HeroService;
use PHPUnit\Framework\TestCase;

class HeroServiceTest extends TestCase
{
    public function testCalculateFirstStriker()
    {
        $heroService = new HeroService();

        $hero = new Hero();
        $hero->setHealth(100);
        $hero->setStrength(70);
        $hero->setSpeed(50);

        $beast = new Beast();
        $beast->setHealth(90);
        $beast->setStrength(70);
        $beast->setSpeed(60);

        list($striker, $defender) = $heroService->calculateFirsStriker($hero, $beast);

        $this->assertTrue($striker instanceof Beast);
        $this->assertTrue($defender instanceof Hero);

        $hero->setSpeed(50);
        $hero->setLuck(15);
        $beast->setSpeed(50);
        $beast->setLuck(10);

        list($striker, $defender) = $heroService->calculateFirsStriker($hero, $beast);

        $this->assertTrue($striker instanceof Hero);
        $this->assertTrue($defender instanceof Beast);

        $hero->setSpeed(60);
        $beast->setSpeed(50);

        list($striker, $defender) = $heroService->calculateFirsStriker($hero, $beast);

        $this->assertTrue($striker instanceof Hero);
        $this->assertTrue($defender instanceof Beast);
    }

    public function testChangeStriker()
    {
        $heroService = new HeroService();

        $hero = new Hero();
        $hero->setHealth(100);
        $hero->setStrength(70);
        $hero->setSpeed(50);

        $beast = new Beast();
        $beast->setHealth(90);
        $beast->setStrength(70);
        $beast->setSpeed(60);

        list($striker, $defender) = $heroService->changeStriker($hero, $beast);

        $this->assertTrue($striker . "" == $beast . "");
        $this->assertTrue($defender . "" == $hero . "");
    }

    public function testStrike()
    {
        $heroService = new HeroService();

        $hero = new Hero();
        $hero->setHealth(100);
        $hero->setStrength(70);
        $hero->setSpeed(50);

        $beast = new Beast();
        $beast->setHealth(90);
        $beast->setStrength(70);
        $beast->setSpeed(60);

        list($striker, $defender) = $heroService->strike($hero, $beast);

        $this->assertTrue($beast->getHealth() > $defender->getHealth());
    }

    function testCanContinueFight()
    {
        $heroService = new HeroService();
        $hero = new Hero();
        $hero->setHealth(1);
        $this->assertTrue($heroService->canContinueFight($hero));

        $hero->setHealth(0);
        $this->assertFalse($heroService->canContinueFight($hero));

        $hero->setHealth(-1);
        $this->assertFalse($heroService->canContinueFight($hero));
    }
}